require('dotenv').config()
const express = require('express')
const app = express()
const Table = require('./models/table')

app.use(express.json())

app.get('/api/tables', (request, response) => {
  console.log('app.get');
  Table.find({}).then(result => {
    console.log(`all tables ${result}`)
    response.json(result)
  }).catch(error => {
    console.log('Did fail to fetch tables. Error:', error);
  })
})

app.delete('/api/tables/:id', (request, response) => {
  Table.findByIdAndRemove(request.params.id)
    .then(result => {
      response.status(204).end()
    })
    .catch(error => next(error))
})

app.post('/api/tables', (request, response) => {
  const body = request.body

  if (!body.name || !body.tasks) {
    return response.status(400).json({
      error: 'content missing'
    })
  }

  const table = new Table({
    name: body.name,
    tasks: body.tasks
  })

  table.save().then(savedTable => {
    response.json(savedTable)
  })
})

app.put('/api/tables/:id', (request, response, next) => {
  const body = request.body

  const table = {
    name: body.name,
    tasks: body.tasks
  }

  console.log(request.params.id);

  Table.findByIdAndUpdate(request.params.id, table, { new: true })
    .then(updatedTable => {
      console.log('Updated table', updatedTable);
      response.json(updatedTable)
    })
    .catch(error => next(error))
})

// Middlewares(now error handling) are necessary to be bottom of the file 
const unknownEndpoint = (request, response) => {
  response.status(404).send({ error: 'unknown endpoint' })
}

app.use(unknownEndpoint)

const errorHandler = (error, request, response, next) => {
  console.log(error.message)

  if (error.name === 'CastError') {
    return response.status(400).send({ error: 'malformatted id' })
  }

  next(error)
}

app.use(errorHandler)

const PORT = process.env.PORT
app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
})